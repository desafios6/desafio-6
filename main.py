def find_index(array):
    max_sequence = 0
    index = -1 
    aux_zero = -1
    zero = -1

    for i in range(len(array)):
        if(array[i]==0):
            if (i - aux_zero) > max_sequence:
                index = zero
                max_sequence = i - aux_zero
            aux_zero = zero
            zero = i
    
    if(len(array) - aux_zero) > max_sequence:
        return zero
    
    return index+1 
            
#para condizer com o exemplo dado no enunciado, considero aqui que o vetor inicia da posição 1, não 0 como o habitual
sequence = [0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1]
# posição   1, 2, 3, 4, 5, 6......

print(find_index(sequence))